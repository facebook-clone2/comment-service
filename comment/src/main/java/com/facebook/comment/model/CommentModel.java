package com.facebook.comment.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@Data
public class CommentModel {
    private int id;

    private int userId;

    private String postId;

    private String content;

    private LocalDateTime time;

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public String getPostId() {
        return postId;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
